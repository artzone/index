package pl.arturkb.artzone.index;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rometools.utils.Strings;
import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Configuration {

  private static final Logger LOGGER = LogManager.getLogger(Configuration.class);
  private static final String ELASTIC_HOSTNAMES_NOT_NULL = "ElasticSearch hostnames can't be "
      + "empty or null";
  private static final String ELASTIC_PORT_NOT_NULL = "ElasticSearch port can't be null";
  private static final String BATCH_SIZE_NOT_NULL = "Batch size can't be null";

  private final String elasticHostnames;
  private final Integer elasticPort;
  private final String elasticUsername;
  private final String elasticUsernamePassword;

  private final Integer batchSize;

  /**
   * The class that represents the configuration.
   *
   * @param elasticHostnames        the hostnames for the Elastic Search
   * @param elasticPort             the Elastic Search port number
   * @param elasticUsername         the Elastic Search user name
   * @param elasticUsernamePassword the Elastic Search user password
   * @param batchSize               the batch size to bu used when indexing document pages
   */
  @JsonCreator(mode = Mode.PROPERTIES)
  public Configuration(
      @JsonProperty("elasticHostnames") String elasticHostnames,
      @JsonProperty("elasticPort") Integer elasticPort,
      @JsonProperty("elasticUsername") String elasticUsername,
      @JsonProperty("elasticUsernamePassword") String elasticUsernamePassword,
      @JsonProperty("batchSize") Integer batchSize) throws ConfigurationException {
    this.elasticHostnames = elasticHostnames;
    this.elasticPort = elasticPort;
    this.elasticUsername = elasticUsername;
    this.elasticUsernamePassword = elasticUsernamePassword;
    this.batchSize = batchSize;
  }

  public String getElasticHostnames() {
    return elasticHostnames;
  }

  public Integer getElasticPort() {
    return elasticPort;
  }

  public String getElasticUsername() {
    return elasticUsername;
  }

  public String getElasticUsernamePassword() {
    return elasticUsernamePassword;
  }

  public Integer getBatchSize() {
    return batchSize;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Configuration that = (Configuration) o;
    return Objects.equals(elasticHostnames, that.elasticHostnames)
        && Objects.equals(elasticPort, that.elasticPort)
        && Objects.equals(elasticUsername, that.elasticUsername)
        && Objects.equals(elasticUsernamePassword, that.elasticUsernamePassword)
        && Objects.equals(batchSize, that.batchSize);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(elasticHostnames, elasticPort, elasticUsername, elasticUsernamePassword, batchSize);
  }

  /**
   * Validate configuration.
   *
   * @throws ConfigurationException throws ConfigurationException when configuration is not valid
   */
  public void validate() throws ConfigurationException {
    if (Strings.isEmpty(elasticHostnames)) {
      LOGGER.error(ELASTIC_HOSTNAMES_NOT_NULL);
      throw new ConfigurationException(ELASTIC_HOSTNAMES_NOT_NULL);
    }
    if (Objects.isNull(elasticPort)) {
      LOGGER.error(ELASTIC_PORT_NOT_NULL);
      throw new ConfigurationException(ELASTIC_PORT_NOT_NULL);
    }
    if (Objects.isNull(batchSize)) {
      LOGGER.error(BATCH_SIZE_NOT_NULL);
      throw new ConfigurationException(BATCH_SIZE_NOT_NULL);
    }
  }
}
