package pl.arturkb.artzone.index.elasticsearch;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.elasticsearch.client.RestHighLevelClient;
import pl.arturkb.artzone.elasticsearch.RestHighLevelClientFactory;
import pl.arturkb.artzone.index.Configuration;

public class RestHighLevelClientProvider implements Provider<RestHighLevelClient> {

  private final Configuration configuration;

  @Inject
  RestHighLevelClientProvider(Configuration configuration) {
    this.configuration = configuration;
  }

  @Override
  public RestHighLevelClient get() {
    return RestHighLevelClientFactory.make()
        .withHostnames(configuration.getElasticHostnames())
        .withPort(configuration.getElasticPort())
        .withUsername(configuration.getElasticUsername())
        .withPassword(configuration.getElasticUsernamePassword())
        .build();
  }
}
