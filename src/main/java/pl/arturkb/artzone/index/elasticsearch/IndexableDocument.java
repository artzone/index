package pl.arturkb.artzone.index.elasticsearch;

import org.elasticsearch.common.xcontent.XContentBuilder;
import pl.arturkb.artzone.elasticsearch.index.AbstractIndexableDocument;
import pl.arturkb.artzone.index.domain.DocumentDto;

public class IndexableDocument extends AbstractIndexableDocument<DocumentDto> {

  public IndexableDocument(DocumentDto source) {
    super(source);
  }

  @Override
  public XContentBuilder getMapping() {
    //Default mapping for now
    return null;
  }
}