package pl.arturkb.artzone.index.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.Objects;
import pl.arturkb.artzone.index.DateTimeUtils;

@JsonDeserialize(builder = DocumentDto.Builder.class)
public class DocumentDto {

  private final String path;
  private final String name;
  private final String extension;
  @JsonFormat(pattern = DateTimeUtils.PATTERN)
  private final Date creationTime;
  @JsonFormat(pattern = DateTimeUtils.PATTERN)
  private final Date lastModifiedTime;
  private final long size;
  private final int pageNumber;
  private final int numberOfPages;
  private final String content;
  private final String mimeType;
  private final String mimeSubType;

  private DocumentDto(Builder builder) {
    this.path = builder.path;
    this.name = builder.name;
    this.extension = builder.extension;
    this.creationTime = builder.creationTime;
    this.lastModifiedTime = builder.lastModifiedTime;
    this.size = builder.size;
    this.pageNumber = builder.pageNumber;
    this.numberOfPages = builder.numberOfPages;
    this.content = builder.content;
    this.mimeSubType = builder.mimeSubType;
    this.mimeType = builder.mimeType;
  }

  public String getPath() {
    return path;
  }

  public String getName() {
    return name;
  }

  public String getExtension() {
    return extension;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public Date getLastModifiedTime() {
    return lastModifiedTime;
  }

  public long getSize() {
    return size;
  }

  public int getPageNumber() {
    return pageNumber;
  }

  public int getNumberOfPages() {
    return numberOfPages;
  }

  public String getContent() {
    return content;
  }

  public String getMimeType() {
    return mimeType;
  }

  public String getMimeSubType() {
    return mimeSubType;
  }

  public static Builder make() {
    return new Builder();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DocumentDto that = (DocumentDto) o;
    return size == that.size
        && pageNumber == that.pageNumber
        && numberOfPages == that.numberOfPages
        && Objects.equals(path, that.path)
        && Objects.equals(name, that.name)
        && Objects.equals(extension, that.extension)
        && Objects.equals(creationTime, that.creationTime)
        && Objects.equals(lastModifiedTime, that.lastModifiedTime)
        && Objects.equals(content, that.content)
        && Objects.equals(mimeSubType, that.mimeSubType)
        && Objects.equals(mimeType, that.mimeType);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(path, name, extension, creationTime, lastModifiedTime, size, pageNumber,
            numberOfPages, content, mimeSubType, mimeType);
  }

  public static class Builder {

    private String path;
    private String name;
    private String extension;
    private Date creationTime;
    private Date lastModifiedTime;
    private long size;
    private int pageNumber;
    private int numberOfPages;
    private String content;
    private String mimeSubType;
    private String mimeType;

    public Builder withPath(String path) {
      this.path = path;
      return this;
    }

    public Builder withName(String name) {
      this.name = name;
      return this;
    }

    public Builder withExtension(String extension) {
      this.extension = extension;
      return this;
    }

    @JsonFormat(pattern = DateTimeUtils.PATTERN)
    public Builder withCreationTime(Date creationTime) {
      this.creationTime = creationTime;
      return this;
    }

    @JsonFormat(pattern = DateTimeUtils.PATTERN)
    public Builder withLastModifiedTime(Date lastModifiedTime) {
      this.lastModifiedTime = lastModifiedTime;
      return this;
    }

    public Builder withSize(long size) {
      this.size = size;
      return this;
    }

    public Builder withPageNumber(int pageNumber) {
      this.pageNumber = pageNumber;
      return this;
    }

    public Builder withNumberOfPages(int numberOfPages) {
      this.numberOfPages = numberOfPages;
      return this;
    }

    public Builder withContent(String content) {
      this.content = content;
      return this;
    }

    public Builder withMimeSubType(String mimeSubType) {
      this.mimeSubType = mimeSubType;
      return this;
    }

    public Builder withMimeType(String mimeType) {
      this.mimeType = mimeType;
      return this;
    }

    public DocumentDto build() {
      return new DocumentDto(this);
    }
  }
}