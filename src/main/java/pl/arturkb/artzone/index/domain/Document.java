package pl.arturkb.artzone.index.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class represents document to be processed by Indexer.
 */
public class Document {

  private FileInfo fileInfo;
  private final Set<Page> pages = new HashSet<>();
  private String mimeType;
  private String mimeSubType;
  private TextPlainMetadata textPlainMetadata;

  private Boolean isIndexed = false;
  private Boolean isPreprocessed = false;
  private Boolean isDetected = false;
  private Boolean isProcessed = false;


  /**
   * Getter for pages.
   *
   * @return the pages
   */
  public Set<Page> getPages() {
    return pages;
  }

  /**
   * Check that document has more than one page.
   *
   * @return true if document is multipage, false otherwise
   */
  public boolean isMultiPage() {
    return pages.size() > 1;
  }

  /**
   * Retrieve the page of given number.
   *
   * @param pageNumber the page number to bre retrieved
   * @return the page if found, null otherwise
   */
  public Page getPage(int pageNumber) {
    final Set<Page> result = pages.stream().filter(page -> page.getNumber() == pageNumber)
        .collect(Collectors.toSet());
    if (result.size() > 1) {
      return null;
    } else {
      return result.stream().findFirst().orElse(null);
    }
  }

  /**
   * Add the page to the document.
   *
   * @param page the page to be added
   */
  public void addPage(Page page) {
    pages.add(page);
  }

  /**
   * Return the number of pages for the document.
   *
   * @return the number of pages of this document.
   */
  public int getNumberOfPages() {
    return pages.size();
  }

  /**
   * The getter for fileInfo.
   *
   * @return the fileInfo
   */
  public FileInfo getFileInfo() {
    return fileInfo;
  }

  /**
   * The setter for fileInfo.
   *
   * @param fileInfo the fileInfo
   */
  public void setFileInfo(FileInfo fileInfo) {
    this.fileInfo = fileInfo;
  }

  /**
   * The getter for mimeType.
   *
   * @return the mimeType
   */
  public String getMimeType() {
    return mimeType;
  }

  /**
   * Setter for mimeType.
   *
   * @param mimeType the mimeType
   */
  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  /**
   * The getter for mimeSubType.
   *
   * @return the mimeSubType
   */
  public String getMimeSubType() {
    return mimeSubType;
  }

  /**
   * The setter for mimeSubType.
   *
   * @param mimeSubType the mimeSubType
   */
  public void setMimeSubType(String mimeSubType) {
    this.mimeSubType = mimeSubType;
  }

  /**
   * The getter for textPlainMetadata.
   *
   * @return the textPlainMetadata
   */
  public TextPlainMetadata getTextPlainMetadata() {
    return textPlainMetadata;
  }

  /**
   * The setter for textPlainMetadata.
   *
   * @param textPlainMetadata the textPlainMetadata
   */
  public void setTextPlainMetadata(TextPlainMetadata textPlainMetadata) {
    this.textPlainMetadata = textPlainMetadata;
  }

  /**
   * Get status of being indexed or not.
   *
   * @return true if document is indexed, false otherwise
   */
  public Boolean getIndexed() {
    return isIndexed;
  }

  /**
   * Setter for indexed.
   *
   * @param indexed is indexed
   */
  public void setIndexed(Boolean indexed) {
    isIndexed = indexed;
  }

  /**
   * Get status of being preprocessed or not.
   *
   * @return true if document is preprocessed, false otherwise
   */
  public Boolean getPreprocessed() {
    return isPreprocessed;
  }

  /**
   * Setter for preprocessed.
   *
   * @param preprocessed is preprocessed
   */
  public void setPreprocessed(Boolean preprocessed) {
    isPreprocessed = preprocessed;
  }

  /**
   * Get status of being detected or not.
   *
   * @return true if document is detected, false otherwise
   */
  public Boolean getDetected() {
    return isDetected;
  }

  /**
   * Setter for isDetected.
   *
   * @param detected is detected
   */
  public void setDetected(Boolean detected) {
    isDetected = detected;
  }

  /**
   * Get status of being processed or not.
   *
   * @return true if document is processed, false otherwise
   */
  public Boolean getProcessed() {
    return isProcessed;
  }

  /**
   * Setter for isProcessed.
   *
   * @param processed is processed
   */
  public void setProcessed(Boolean processed) {
    isProcessed = processed;
  }
}