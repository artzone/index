package pl.arturkb.artzone.index.domain;

import static java.util.Objects.nonNull;

import edu.umd.cs.findbugs.annotations.NonNull;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Collectors;

public class DocumentMapper {

  private final Document document;
  private final Set<Page> pages;

  public DocumentMapper(@NonNull Document document) {
    this.document = document;
    pages = new HashSet<>(document.getPages());
  }

  public int getNumberPagesToIndex() {
    return pages.size();
  }

  /**
   * Retrieve documents from pages. The amount of pages is removed after this call.
   *
   * @param amount the amount of pages to retrieve
   * @return the DocumentDTOs
   */
  @NonNull
  public Set<DocumentDto> getDocumentsDto(int amount) {
    final Set<Page> remove = remove(amount);
    pages.removeAll(remove);
    return remove.stream().map(page -> createDto(document, page)).collect(Collectors.toSet());
  }

  /**
   * Create DTO.
   *
   * @param document   the document to create DTO from
   * @param pageNumber the page number to create DTO for
   * @return the DocumentDTO
   */
  @NonNull
  public static DocumentDto createDto(@NonNull Document document, int pageNumber) {
    if (nonNull(document.getFileInfo()) && nonNull(
        document.getPage(pageNumber))) {
      final Page page = document.getPage(pageNumber);
      return DocumentDto.make()
          .withPath(document.getFileInfo().getPath())
          .withName(document.getFileInfo().getName())
          .withCreationTime(document.getFileInfo().getCreationTime())
          .withLastModifiedTime(document.getFileInfo().getLastModifiedTime())
          .withExtension(document.getFileInfo().getExtension())
          .withSize(document.getFileInfo().getSize())
          .withContent(page.getContent())
          .withPageNumber(page.getNumber())
          .withNumberOfPages(document.getNumberOfPages())
          .withMimeType(document.getMimeType())
          .withMimeSubType(document.getMimeSubType())
          .build();
    } else {
      return DocumentDto.make().build();
    }
  }

  /**
   * Create DTO form given page and document.
   *
   * @param document the document to create DTO from
   * @param page     the page to create DTO from
   * @return the DocumentDto
   */
  @NonNull
  public static DocumentDto createDto(@NonNull Document document, @NonNull Page page) {
    if (nonNull(document.getFileInfo())) {
      return DocumentDto.make()
          .withPath(document.getFileInfo().getPath())
          .withName(document.getFileInfo().getName())
          .withCreationTime(document.getFileInfo().getCreationTime())
          .withLastModifiedTime(document.getFileInfo().getLastModifiedTime())
          .withExtension(document.getFileInfo().getExtension())
          .withSize(document.getFileInfo().getSize())
          .withContent(page.getContent())
          .withPageNumber(page.getNumber())
          .withNumberOfPages(document.getNumberOfPages())
          .withMimeType(document.getMimeType())
          .withMimeSubType(document.getMimeSubType())
          .build();
    } else {
      return DocumentDto.make().build();
    }
  }

  /**
   * Create DTOs form given document.
   *
   * @param document the document to create DTO from
   * @return the DocumentDTOs
   */
  @NonNull
  public static Set<DocumentDto> createDtos(@NonNull Document document) {
    return document.getPages().stream().map(page -> createDto(document, page.getNumber()))
        .collect(Collectors.toSet());
  }

  private Set<Page> remove(int amount) {
    final Set<Page> removed = new HashSet<>();
    final Iterator<Page> iterator = pages.iterator();
    int i = amount;
    while (iterator.hasNext() && i > 0) {
      removed.add(iterator.next());
      i--;
    }
    return removed;
  }
}