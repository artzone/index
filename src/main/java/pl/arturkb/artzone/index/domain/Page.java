package pl.arturkb.artzone.index.domain;

import java.util.Objects;

public class Page {

  private final int number;
  private final String content;

  public Page(int number, String content) {
    this.number = number;
    this.content = content;
  }

  public int getNumber() {
    return number;
  }

  public String getContent() {
    return content;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Page page = (Page) o;
    return number == page.number
        && Objects.equals(content, page.content);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, content);
  }
}
