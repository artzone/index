package pl.arturkb.artzone.index.domain;

import edu.umd.cs.findbugs.annotations.NonNull;
import java.util.Date;
import java.util.Objects;

public class FileInfo {

  private final String path;
  private final String name;
  private final String extension;
  private final Date creationTime;
  private final Date lastModifiedTime;
  private final long size;

  /**
   * File info constructor.
   *
   * @param path             the path for the file
   * @param name             the name of the file
   * @param extension        the extension of the file
   * @param creationTime     the creation time
   * @param lastModifiedTime the last modified time
   * @param size             the size of the file
   */
  public FileInfo(@NonNull String path, @NonNull String name, @NonNull String extension,
      @NonNull Date creationTime, @NonNull Date lastModifiedTime, long size) {
    this.path = path;
    this.name = name;
    this.extension = extension;
    this.creationTime = creationTime;
    this.lastModifiedTime = lastModifiedTime;
    this.size = size;
  }

  public String getPath() {
    return path;
  }

  public String getName() {
    return name;
  }

  public String getExtension() {
    return extension;
  }

  public Date getCreationTime() {
    return creationTime;
  }

  public Date getLastModifiedTime() {
    return lastModifiedTime;
  }

  public long getSize() {
    return size;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FileInfo fileInfoDto = (FileInfo) o;
    return size == fileInfoDto.size
        && Objects.equals(path, fileInfoDto.path)
        && Objects.equals(name, fileInfoDto.name)
        && Objects.equals(extension, fileInfoDto.extension)
        && Objects.equals(creationTime, fileInfoDto.creationTime)
        && Objects.equals(lastModifiedTime, fileInfoDto.lastModifiedTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(path, name, extension, creationTime,
        lastModifiedTime, size);
  }
}
