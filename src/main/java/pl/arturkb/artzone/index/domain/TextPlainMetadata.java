package pl.arturkb.artzone.index.domain;

import java.util.Objects;

public class TextPlainMetadata {

  public static final String CONTENT_ENCODING_NAME = "Content-Encoding";
  public static final String CONTENT_TYPE_NAME = "Content-Type";

  private final String contentEncoding;
  private final String contentType;

  public TextPlainMetadata(String contentEncoding, String contentType) {
    this.contentEncoding = contentEncoding;
    this.contentType = contentType;
  }

  public TextPlainMetadata copy() {
    return new TextPlainMetadata(contentEncoding, contentType);
  }

  public String getContentEncoding() {
    return contentEncoding;
  }

  public String getContentType() {
    return contentType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TextPlainMetadata that = (TextPlainMetadata) o;
    return Objects.equals(contentEncoding, that.contentEncoding)
        && Objects.equals(contentType, that.contentType);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentEncoding, contentType);
  }
}
