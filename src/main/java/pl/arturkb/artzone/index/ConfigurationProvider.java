package pl.arturkb.artzone.index;

import com.google.inject.Inject;
import com.google.inject.Provider;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.arturkb.artzone.utils.MappingUtils;

public class ConfigurationProvider implements Provider<Configuration> {

  private static final Logger LOGGER = LogManager.getLogger(ConfigurationProvider.class);
  private static final String CONFIG_FILE_NAME = "config.json";

  private final AppContext appContext;

  @Inject
  ConfigurationProvider(AppContext appContext) {
    this.appContext = appContext;
  }

  @Override
  public Configuration get() {
    try (final InputStream resourceAsStream = this.getClass().getClassLoader()
        .getResourceAsStream(CONFIG_FILE_NAME)) {
      final Configuration configuration = MappingUtils
          .getObject(resourceAsStream, Configuration.class);
      configuration.validate();
      return configuration;
    } catch (FileNotFoundException e) {
      LOGGER.error("Configuration file: {} is missing", CONFIG_FILE_NAME);
      appContext.addExitCode(ExitCode.FILE_NOT_FOUND_EXCEPTION);
    } catch (IOException e) {
      LOGGER.error("Gor IOException", e);
      appContext.addExitCode(ExitCode.IO_EXCEPTION);
    } catch (ConfigurationException e) {
      LOGGER.error("Validation of configuration failed", e);
      appContext.addExitCode(ExitCode.CONFIGURATION_VALIDATION);
    }
    return null;
  }
}
