package pl.arturkb.artzone.index;


import com.google.inject.AbstractModule;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.tika.detect.Detector;
import org.apache.tika.mime.MimeTypes;
import org.apache.tika.parser.txt.TXTParser;
import org.elasticsearch.client.RestHighLevelClient;
import picocli.CommandLine;
import picocli.CommandLine.IFactory;
import pl.arturkb.artzone.index.elasticsearch.RestHighLevelClientProvider;
import pl.arturkb.artzone.index.processor.ApplicationPdfProcessor;
import pl.arturkb.artzone.index.processor.Engine;
import pl.arturkb.artzone.index.processor.FilePreprocessor;
import pl.arturkb.artzone.index.processor.TextPlainProcessor;
import pl.arturkb.artzone.index.repository.FileIndexRepository;
import pl.arturkb.artzone.index.repository.FileIndexRepositoryProvider;

public class GuiceFactory implements IFactory {

  private final Injector injector = Guice.createInjector(
      new Processor(),
      new ElasticSearch());

  @Override
  public <K> K create(Class<K> cls) throws Exception {

    try {
      return injector.getInstance(cls);
    } catch (ConfigurationException ex) { // no implementation found in Guice configuration
      return CommandLine.defaultFactory().create(cls); // fallback if missing
    }
  }

  static class Processor extends AbstractModule {

    @Override
    protected void configure() {
      bind(AppContext.class).asEagerSingleton();
      bind(Configuration.class).toProvider(ConfigurationProvider.class).asEagerSingleton();
      bind(Detector.class).to(MimeTypes.class).asEagerSingleton();
      bind(TXTParser.class).asEagerSingleton();

      bind(FilePreprocessor.class);
      bind(TextPlainProcessor.class);
      bind(ApplicationPdfProcessor.class);
      bind(Engine.class);
    }
  }

  static class ElasticSearch extends AbstractModule {

    @Override
    protected void configure() {
      bind(RestHighLevelClient.class).toProvider(RestHighLevelClientProvider.class)
          .asEagerSingleton();
      bind(FileIndexRepository.class).toProvider(FileIndexRepositoryProvider.class)
          .asEagerSingleton();
    }
  }
}