package pl.arturkb.artzone.index.command;

import com.google.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.ElasticsearchStatusException;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import pl.arturkb.artzone.index.AppContext;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.Texts;
import pl.arturkb.artzone.index.processor.Engine;
import pl.arturkb.artzone.index.repository.FileIndexRepository;

@Command(
    name = "index",
    version = "Index ver.0.1",
    header = "%nElasticSearch indexer.%n",
    footer = "%nwww.arturkb.pl 2020.%n",
    mixinStandardHelpOptions = true)
public class Index implements Callable<Integer> {

  private static final Logger LOGGER = LogManager.getLogger(Index.class);

  @Option(
      names = {"-v", "--verbose"},
      description = "Verbose output, false default")
  private final Boolean verbose = false;

  @Parameters(
      paramLabel = "Path",
      arity = "1..1",
      description = "directory where to start indexing")
  private String pathString;

  @Option(
      names = {"-n", "--name"},
      required = true,
      description = "Index name to bu used for indexing")
  private String indexName;

  @Option(
      names = {"-d", "--dropIndex"},
      description = "Delete index if given index exists, false default")
  private final Boolean dropIndex = false;

  private final Engine engine;
  private final FileIndexRepository fileIndexRepository;
  private final AppContext appContext;

  @Inject
  Index(FileIndexRepository fileIndexRepository, Engine engine, AppContext appContext) {
    this.fileIndexRepository = fileIndexRepository;
    this.engine = engine;
    this.appContext = appContext;
  }

  @Override
  public Integer call() {
    if (appContext.hasFatalErrorExitCode()) {
      return ExitCode.ABNORMAL.getValue();
    }

    final ExitCode initIndexExitCode = initIndex();
    if (!initIndexExitCode.equals(ExitCode.NORMAL)) {
      return initIndexExitCode.getValue();
    }

    final Path path = Paths.get(pathString);
    if (Files.notExists(path)) {
      LOGGER.warn("Path: {} does not exists!", pathString);
      System.out.print(pathString + " : ");
      System.out.println(Texts.DOES_NOT_EXISTS.getValue());
      return ExitCode.PATH_NOT_EXISTS.getValue();
    }

    try (Stream<Path> walk = Files.walk(path)) {
      engine.setIndexName(indexName);
      engine.setVerbose(verbose);
      walk.filter(Files::isRegularFile).forEach(engine::run);

    } catch (IOException e) {
      LOGGER.error("Got IOException for {}", pathString, e);
      return ExitCode.IO_EXCEPTION.getValue();
    }

    if (!appContext.hasFatalErrorExitCode()) {
      return ExitCode.NORMAL.getValue();
    } else {
      return ExitCode.ABNORMAL.getValue();
    }
  }

  public Boolean getVerbose() {
    return verbose;
  }

  private ExitCode initIndex() {
    try {
      if (fileIndexRepository.indexAdmin(indexName).indexExists()) {
        if (dropIndex) {
          fileIndexRepository.indexAdmin(indexName).deleteIndex();
        } else {
          LOGGER.error("Index already exists {} ", indexName);
          return ExitCode.ELASTIC_INDEX_ALREADY_EXISTS;
        }
      }
      fileIndexRepository.createIndex(indexName, null, null);
    } catch (ElasticsearchStatusException | IOException e) {
      LOGGER.error("Fatal error during index {} creation", indexName, e);
      return ExitCode.ELASTIC_INIT_ERROR;
    }
    return ExitCode.NORMAL;
  }
}
