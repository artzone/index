package pl.arturkb.artzone.index.repository;

import org.elasticsearch.client.RestHighLevelClient;
import pl.arturkb.artzone.elasticsearch.index.IndexRepositoryImpl;
import pl.arturkb.artzone.index.elasticsearch.IndexableDocument;

public class FileIndexRepository extends IndexRepositoryImpl<IndexableDocument> {

  public FileIndexRepository(RestHighLevelClient client) {
    super(client);
  }
}
