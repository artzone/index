package pl.arturkb.artzone.index.repository;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.elasticsearch.client.RestHighLevelClient;

public class FileIndexRepositoryProvider implements Provider<FileIndexRepository> {

  private final RestHighLevelClient restHighLevelClient;

  @Inject
  FileIndexRepositoryProvider(RestHighLevelClient restHighLevelClient) {
    this.restHighLevelClient = restHighLevelClient;
  }

  @Override
  public FileIndexRepository get() {
    return new FileIndexRepository(restHighLevelClient);
  }
}
