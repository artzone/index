package pl.arturkb.artzone.index.processor;

import static java.util.Objects.nonNull;

import com.google.inject.Inject;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.detect.Detector;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.mime.MediaType;
import pl.arturkb.artzone.index.AppContext;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.domain.FileInfo;

/**
 * Preprocessor class that file page with info about file.
 */
public class FilePreprocessor {

  private static final Logger LOGGER = LogManager.getLogger(FilePreprocessor.class);
  private final Detector detector;
  private final AppContext appContext;

  @Inject
  FilePreprocessor(Detector detector, AppContext appContext) {
    this.appContext = appContext;
    this.detector = detector;
  }

  /**
   * Fill page with info about file.
   *
   * @param processorContext the processor context
   * @param path             the path to process
   */
  public void process(@NonNull ProcessorContext processorContext, @NonNull Path path) {
    BasicFileAttributeView fileAttributeView = Files
        .getFileAttributeView(path, BasicFileAttributeView.class);
    final FileInfo fileInfo;
    try {
      BasicFileAttributes basicFileAttributes = fileAttributeView.readAttributes();
      fileInfo = new FileInfo(
          path.toString(),
          path.getFileName().toString(),
          getExtension(path.getFileName().toString()),
          new Date(basicFileAttributes.creationTime().toMillis()),
          new Date(basicFileAttributes.lastModifiedTime().toMillis()),
          basicFileAttributes.size());
      try (InputStream stream = new BufferedInputStream(
          new FileInputStream(new File(path.toString())))) {
        final MediaType mediaType = detector.detect(stream, new Metadata());
        if (nonNull(mediaType)) {
          processorContext.getDocument().setMimeType(mediaType.getType());
          processorContext.getDocument().setMimeSubType(mediaType.getSubtype());
          processorContext.getDocument().setDetected(true);
        }
      } catch (FileNotFoundException e) {
        LOGGER.error("Got FileNotFoundException on {}", path.toString(), e);
        appContext.addExitCode(ExitCode.FILE_NOT_FOUND_EXCEPTION);
        return;
      } catch (IOException e) {
        LOGGER.error("Got IOException on {}", path.toString(), e);
        appContext.addExitCode(ExitCode.IO_EXCEPTION);
        return;
      }
    } catch (IOException e) {
      LOGGER.error("Got IOException on {}", path.toString(), e);
      appContext.addExitCode(ExitCode.IO_EXCEPTION);
      return;
    }
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setPreprocessed(true);
    processorContext.appendMessageToOutput(path.toString() + " ");
    processorContext.appendMessageToOutput(
        "(" + processorContext.getDocument().getMimeType() + "/" + processorContext.getDocument()
            .getMimeSubType() + ")");
    appContext.addExitCode(ExitCode.NORMAL);
  }

  private String getExtension(String fileName) {
    String extension = "";
    int i = fileName.lastIndexOf('.');
    if (i > 0) {
      extension = fileName.substring(i + 1);
    }
    return extension;
  }
}