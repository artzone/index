package pl.arturkb.artzone.index.processor;

import static java.util.Objects.nonNull;

import com.google.inject.Inject;
import edu.umd.cs.findbugs.annotations.NonNull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.txt.TXTParser;
import org.apache.tika.sax.ContentHandlerDecorator;
import org.xml.sax.SAXException;
import pl.arturkb.artzone.index.AppContext;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.domain.Page;
import pl.arturkb.artzone.index.domain.TextPlainMetadata;

public class TextPlainProcessor implements Processor {

  private static final Logger LOGGER = LogManager.getLogger(TextPlainProcessor.class);
  private static final String PROCESSOR_NAME = "[TEXT_PLAIN_PROCESSOR]";
  private final TXTParser txtParser;
  private final AppContext appContext;

  @Inject
  TextPlainProcessor(TXTParser txtParser, AppContext appContext) {
    this.txtParser = txtParser;
    this.appContext = appContext;
  }

  @Override
  public void process(@NonNull ProcessorContext processorContext) {
    if (match(processorContext)) {
      final File file = new File(processorContext.getDocument().getFileInfo().getPath());
      if (file.exists()) {
        try {
          parseToPlainTextChunks(file, processorContext);
        } catch (IOException e) {
          LOGGER.error("Got IOException on {}",
              processorContext.getDocument().getFileInfo().getPath(), e);
          appContext.addExitCode(ExitCode.IO_EXCEPTION);
        } catch (SAXException e) {
          LOGGER.error("Got SAXException on {}",
              processorContext.getDocument().getFileInfo().getPath(), e);
          appContext.addExitCode(ExitCode.SAX_EXCEPTION);
        } catch (TikaException e) {
          LOGGER.error("Got TikaException on {}",
              processorContext.getDocument().getFileInfo().getPath(), e);
          appContext.addExitCode(ExitCode.TIKA_EXCEPTION);
        }
      }
      processorContext.appendMessageToOutput(PROCESSOR_NAME);
      appContext.addExitCode(ExitCode.NORMAL);
    }
  }

  @Override
  public boolean match(@NonNull ProcessorContext processorContext) {
    if (!appContext.hasFatalErrorExitCode() && nonNull(processorContext.getDocument())) {
      return SupportedMimeType.TEXT_PLAIN
          .equals(SupportedMimeType.fromString(
              processorContext.getDocument().getMimeType(),
              processorContext.getDocument().getMimeSubType()));

    }
    return false;
  }

  /**
   * Parse file as plain text in chunks that are 4096 bytes big.
   *
   * @param file             the file to parse
   * @param processorContext the processor context to be used
   * @throws IOException   throws IOException when error on IO occurs
   * @throws SAXException  throws SAXException on SAX parser
   * @throws TikaException throws TikaException on Tika error
   */
  protected void parseToPlainTextChunks(File file, ProcessorContext processorContext)
      throws IOException, SAXException, TikaException {
    AtomicInteger pageNumber = new AtomicInteger(0);

    ContentHandlerDecorator handler = new ContentHandlerDecorator() {
      @Override
      public void characters(char[] ch, int start, int length) {
        String thisStr = new String(ch, start, length);
        if (length > 0) {
          processorContext.getDocument().addPage(new Page(pageNumber.incrementAndGet(), thisStr));
        }
      }
    };
    parse(file, processorContext, handler);
  }

  private void parse(File file, ProcessorContext processorContext,
      ContentHandlerDecorator handler)
      throws TikaException, SAXException, IOException {
    final ParseContext parseContext = new ParseContext();
    final Metadata metadata = new Metadata();
    try (InputStream stream = new FileInputStream(file)) {
      txtParser.parse(stream, handler, metadata, parseContext);
      final TextPlainMetadata textPlainMetadata = new TextPlainMetadata(
          metadata.get(TextPlainMetadata.CONTENT_ENCODING_NAME),
          metadata.get(TextPlainMetadata.CONTENT_TYPE_NAME));
      processorContext.getDocument().setTextPlainMetadata(textPlainMetadata);
      processorContext.getDocument().setProcessed(true);
    }
  }
}
