package pl.arturkb.artzone.index.processor;

import pl.arturkb.artzone.index.domain.Document;

public class ProcessorContext {

  private final Document document;
  private final StringBuilder output;

  /**
   * The processors context.
   */
  ProcessorContext() {
    this.document = new Document();
    this.output = new StringBuilder();
  }

  public void appendMessageToOutput(String message) {
    output.append(message);
  }

  public String getMessages() {
    return output.toString();
  }

  public Document getDocument() {
    return document;
  }
}
