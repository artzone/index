package pl.arturkb.artzone.index.processor;

import com.google.common.collect.Sets;
import com.google.inject.Inject;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.rest.RestStatus;
import pl.arturkb.artzone.index.AppContext;
import pl.arturkb.artzone.index.Configuration;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.Texts;
import pl.arturkb.artzone.index.domain.Document;
import pl.arturkb.artzone.index.domain.DocumentMapper;
import pl.arturkb.artzone.index.elasticsearch.IndexableDocument;
import pl.arturkb.artzone.index.repository.FileIndexRepository;

public class Engine {

  private static final Logger LOGGER = LogManager.getLogger(Engine.class);

  private final FileIndexRepository fileIndexRepository;
  private final FilePreprocessor filePreprocessor;
  private final Set<Processor> processors;
  private final Configuration configuration;
  private Boolean verbose;
  private String indexName;
  private final AppContext appContext;

  /**
   * Constructor for the Engine.
   *
   * @param fileIndexRepository     the ES repository
   * @param textPlainProcessor      the text plain processor
   * @param applicationPdfProcessor the application PDF processor
   * @param filePreprocessor        the file preprocessor
   */
  @Inject
  Engine(FileIndexRepository fileIndexRepository, TextPlainProcessor textPlainProcessor,
      ApplicationPdfProcessor applicationPdfProcessor, FilePreprocessor filePreprocessor,
      Configuration configuration, AppContext appContext) {
    this.fileIndexRepository = fileIndexRepository;
    this.processors = Sets.newHashSet(
        textPlainProcessor,
        applicationPdfProcessor);
    this.filePreprocessor = filePreprocessor;
    this.configuration = configuration;
    this.appContext = appContext;
  }


  public void setVerbose(Boolean verbose) {
    this.verbose = verbose;
  }

  public void setIndexName(String indexName) {
    this.indexName = indexName;
  }

  /**
   * Process all processor and pre-processor.
   *
   * @param path the path
   */
  public void run(Path path) {
    ProcessorContext processorContext = new ProcessorContext();
    filePreprocessor.process(processorContext, path);
    for (Processor processor : processors) {
      processor.process(processorContext);
    }
    indexDocument(processorContext);
    printOutput(processorContext);
  }

  private void printOutput(ProcessorContext processorContext) {
    if (verbose) {
      if (processorContext.getDocument().getIndexed()) {
        System.out.println(Texts.INDEXED.getValue() + processorContext.getMessages());
      } else {
        System.out.println(Texts.NOT_INDEXED.getValue() + processorContext.getMessages());
      }
    }
  }

  private void indexDocument(ProcessorContext processorContext) {
    if (appContext.hasFatalErrorExitCode()) {
      return;
    }
    if (processorContext.getDocument().isMultiPage()) {
      try {
        indexMultiplePages(processorContext);
      } catch (Exception e) {
        LOGGER.error("Error during indexing file {}",
            processorContext.getDocument().getFileInfo().getPath(), e);
        appContext.addExitCode(ExitCode.ELASTIC_INDEXING_ERROR);
      }
    } else {
      try {
        indexSinglePage(processorContext);
      } catch (Throwable throwable) {
        LOGGER.error("Error during indexing file {}",
            processorContext.getDocument().getFileInfo().getPath(),
            throwable);
        appContext.addExitCode(ExitCode.ELASTIC_INDEXING_ERROR);
      }
    }
  }

  private void indexMultiplePages(ProcessorContext processorContext) throws Exception {
    Optional<BulkResponse> bulkItemResponses;
    boolean resultNormal = true;
    if (processorContext.getDocument().getNumberOfPages() > configuration.getBatchSize()) {
      final DocumentMapper documentMapper = new DocumentMapper(processorContext.getDocument());
      do {
        final Set<IndexableDocument> batch = documentMapper
            .getDocumentsDto(configuration.getBatchSize()).stream().map(
                IndexableDocument::new).collect(Collectors.toSet());

        bulkItemResponses = fileIndexRepository.index(indexName).indexDocumentsSync(batch);
        resultNormal = resultNormal && hasNotFailures(bulkItemResponses);
      } while (documentMapper.getNumberPagesToIndex() > 0);

    } else {
      bulkItemResponses = fileIndexRepository
          .index(indexName)
          .indexDocumentsSync(mapToIndexableDocuments(processorContext.getDocument()));
      resultNormal = resultNormal && hasNotFailures(bulkItemResponses);
    }
    if (resultNormal) {
      processorContext.getDocument().setIndexed((true));
    }
  }

  private void indexSinglePage(ProcessorContext processorContext) throws Throwable {
    final IndexResponse indexResponse = fileIndexRepository.index(indexName)
        .indexDocumentSync(mapToIndexableDocument(processorContext.getDocument()));
    final RestStatus status = indexResponse.status();
    if (Objects.nonNull(status) && status.equals(RestStatus.CREATED)) {
      processorContext.getDocument().setIndexed((true));
    }
  }

  @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
  private boolean hasNotFailures(Optional<BulkResponse> bulkItemResponses) {
    return bulkItemResponses.isPresent() && !bulkItemResponses.get().hasFailures();
  }

  private IndexableDocument mapToIndexableDocument(Document document) {
    return new IndexableDocument(DocumentMapper.createDto(document, 1));
  }

  private List<IndexableDocument> mapToIndexableDocuments(Document document) {
    return DocumentMapper.createDtos(document).stream()
        .map(IndexableDocument::new).collect(Collectors.toList());
  }
}