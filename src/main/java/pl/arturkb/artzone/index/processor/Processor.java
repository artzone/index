package pl.arturkb.artzone.index.processor;

import edu.umd.cs.findbugs.annotations.NonNull;

public interface Processor {

  /**
   * Process the file with given processor context.
   *
   * @param processorContext the given processor context
   */
  void process(@NonNull ProcessorContext processorContext);

  /**
   * Chek that given processor match file type.
   *
   * @param processorContext the processor context
   * @return if file match the processor type then true, false otherwise
   */
  boolean match(@NonNull ProcessorContext processorContext);

}
