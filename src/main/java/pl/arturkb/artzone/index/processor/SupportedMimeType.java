package pl.arturkb.artzone.index.processor;

public enum SupportedMimeType {
  TEXT_PLAIN("text", "plain"),
  APPLICATION_PDF("application", "pdf");

  private final String type;
  private final String subType;

  SupportedMimeType(String type, String subtype) {
    this.type = type;
    this.subType = subtype;
  }

  public String getType() {
    return type;
  }

  public String getSubType() {
    return subType;
  }

  /**
   * If value match enum value then Crete enum form value. Null otherwise.
   *
   * @param type    the type of mimetype
   * @param subtype the subtype of mimetype
   * @return the Enum form value
   */
  public static SupportedMimeType fromString(String type, String subtype) {
    for (SupportedMimeType b : SupportedMimeType.values()) {
      if (b.type.equalsIgnoreCase(type) && b.subType.equalsIgnoreCase(subtype)) {
        return b;
      }
    }
    return null;
  }
}
