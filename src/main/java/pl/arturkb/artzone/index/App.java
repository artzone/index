package pl.arturkb.artzone.index;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import pl.arturkb.artzone.index.command.Index;

public class App {

  private static final Logger LOGGER = LogManager.getLogger(App.class);

  /**
   * The main entry point.
   *
   * @param args arguments
   */
  public static void main(String... args) {
    LOGGER.debug("Starting indexing tool");
    int exitCode = new CommandLine(Index.class, new GuiceFactory()).execute(args);
    LOGGER.debug("Terminating indexer with exit code is {}", exitCode);
    System.exit(exitCode);
  }

}