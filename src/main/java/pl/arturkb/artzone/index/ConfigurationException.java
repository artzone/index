package pl.arturkb.artzone.index;

public class ConfigurationException extends Exception {

  public ConfigurationException(String message) {
    super(message);
  }
}
