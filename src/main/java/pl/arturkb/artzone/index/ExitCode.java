package pl.arturkb.artzone.index;

public enum ExitCode {

  NORMAL(0),
  PATH_NOT_EXISTS(101),
  IS_NOT_FILE(102),
  IO_EXCEPTION(103),
  ELASTIC_INIT_ERROR(104),
  ELASTIC_INDEXING_ERROR(105),
  ELASTIC_INDEX_ALREADY_EXISTS(106),
  SAX_EXCEPTION(107),
  TIKA_EXCEPTION(108),
  FILE_NOT_FOUND_EXCEPTION(109),
  INDEX_HOME_NOT_SET(110),
  CONFIGURATION_VALIDATION(111),
  ABNORMAL(100);

  private final int value;

  /**
   * Enum with value.
   *
   * @param value the value for Enum
   */
  ExitCode(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
