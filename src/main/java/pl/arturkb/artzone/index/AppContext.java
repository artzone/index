package pl.arturkb.artzone.index;

import java.util.HashSet;
import java.util.Set;

public class AppContext {

  private final Set<ExitCode> exitCodes;

  public AppContext() {
    this.exitCodes = new HashSet<>();
  }

  public void addExitCode(ExitCode exitCode) {
    this.exitCodes.add(exitCode);
  }

  /**
   * Check that context contains any exit code that is not NORMAL.
   *
   * @return check that context contains any exit code that is not NORMAL
   */
  public boolean hasFatalErrorExitCode() {
    if (exitCodes.isEmpty()) {
      return false;
    }
    return !exitCodes.stream().allMatch(exitCode -> exitCode.equals(ExitCode.NORMAL));
  }
}
