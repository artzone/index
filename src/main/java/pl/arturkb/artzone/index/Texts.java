package pl.arturkb.artzone.index;

import picocli.CommandLine.Help.Ansi;

public enum Texts {
  DOES_NOT_EXISTS(
      Ansi.AUTO.string("@|bold,red does not exists!|@")),
  INDEXED(Ansi.AUTO.string("@|bold,green indexed: |@")),
  NOT_INDEXED(Ansi.AUTO.string("@|bold,red NOT indexed: |@")),
  IS_NULL(
      Ansi.AUTO.string("@|bold,red is null!|@")),
  IS_NOT_FILE(
      Ansi.AUTO.string("@|bold,red is not file!|@"));

  private final String value;

  Texts(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
