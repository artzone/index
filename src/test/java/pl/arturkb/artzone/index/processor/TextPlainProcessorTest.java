package pl.arturkb.artzone.index.processor;

import static com.google.common.truth.Truth.assertThat;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.Instant;
import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.arturkb.artzone.index.AppContext;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.GuiceFactory;
import pl.arturkb.artzone.index.domain.FileInfo;

public class TextPlainProcessorTest {

  private final GuiceFactory guiceFactory = new GuiceFactory();

  @Test
  public void testProcessBigFileHappyDay() throws Exception {
    final int fileSize = 2_167_737;
    final AppContext appContext = guiceFactory.create(AppContext.class);
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final URL resource = getClass().getClassLoader().getResource("sample-2mb-text-file.txt");
    assertThat(resource).isNotNull();
    final FileInfo fileInfo = new FileInfo(
        resource.getPath(),
        "sample-2mb-text-file",
        "txt",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        fileSize);
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    textPlainProcessor.process(processorContext);
    assertThat(processorContext.getDocument().isMultiPage()).isTrue();
    assertThat(processorContext.getDocument().getPage(1).getContent()).isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentEncoding())
        .isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentType())
        .isNotEmpty();
    assertThat(appContext).isNotNull();
    assertThat(appContext.hasFatalErrorExitCode()).isFalse();
  }

  @Test
  public void testParseToPlainTextBigFileHappyDay() throws Exception {
    final int fileSize = 2_167_737;
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final URL resource = getClass().getClassLoader().getResource("sample-2mb-text-file.txt");
    assertThat(resource).isNotNull();
    final File file = new File(resource.toURI());
    final FileInfo fileInfo = new FileInfo(
        resource.getPath(),
        "sample-2mb-text-file",
        "txt",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        fileSize);
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    textPlainProcessor.parseToPlainTextChunks(file, processorContext);
    assertThat(processorContext.getDocument().isMultiPage()).isTrue();
    assertThat(processorContext.getDocument().getPage(1).getContent()).isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentEncoding())
        .isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentType())
        .isNotEmpty();
  }

  @Test
  public void testParseToPlainTextIoException() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final File file = new File("/home/not_existing_file.test");
    final FileInfo fileInfo = new FileInfo(
        file.getPath(),
        "not_existing_file",
        "test",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        0);
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    Assertions.assertThrows(IOException.class, () ->
        textPlainProcessor.parseToPlainTextChunks(file, processorContext));
  }

  @Test
  public void testParseToPlainTextFirstPage() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final URL resource = getClass().getClassLoader().getResource("FileDtoTestData1.json");
    assertThat(resource).isNotNull();
    final File file = new File(resource.toURI());
    final FileInfo fileInfo = new FileInfo(
        file.getPath(),
        "FileDtoTestData1",
        "json",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        225);
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    textPlainProcessor.parseToPlainTextChunks(file, processorContext);
    assertThat(processorContext.getDocument()).isNotNull();
    assertThat(processorContext.getDocument().getNumberOfPages()).isEqualTo(1);
    assertThat(processorContext.getDocument().getPage(1).getContent()).isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata()).isNotNull();
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentEncoding())
        .isEqualTo("ISO-8859-1");
    assertThat(processorContext.getDocument().getTextPlainMetadata().getContentType())
        .isEqualTo("text/plain; charset=ISO-8859-1");
  }

  @Test
  public void testMatchPositive() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.txt",
        "test",
        "txt",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    processorContext.getDocument().setFileInfo(fileInfo);
    assertThat(textPlainProcessor.match(processorContext)).isTrue();
  }

  @Test
  public void testMatchNegative() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.txt",
        "test",
        "txt",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    processorContext.getDocument().setMimeSubType(SupportedMimeType.APPLICATION_PDF.getSubType());
    processorContext.getDocument().setFileInfo(fileInfo);
    assertThat(textPlainProcessor.match(processorContext)).isFalse();
  }

  @Test
  public void testMatchTypeNull() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.pdf",
        "test",
        "pdf",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeSubType(SupportedMimeType.APPLICATION_PDF.getSubType());
    assertThat(textPlainProcessor.match(processorContext)).isFalse();
  }

  @Test
  public void testMatchSubtypeNull() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.pdf",
        "test",
        "pdf",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    processorContext.getDocument().setMimeType(SupportedMimeType.APPLICATION_PDF.getType());
    assertThat(textPlainProcessor.match(processorContext)).isFalse();
  }

  @Test
  public void testMatchNull() throws Exception {
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.pdf",
        "test",
        "pdf",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    assertThat(textPlainProcessor.match(processorContext)).isFalse();
  }

  @Test
  public void testMatchAppContextHasFatalErrorExitCode() throws Exception {
    final AppContext appContext = guiceFactory.create(AppContext.class);
    appContext.addExitCode(ExitCode.FILE_NOT_FOUND_EXCEPTION);
    final TextPlainProcessor textPlainProcessor = guiceFactory.create(TextPlainProcessor.class);
    final FileInfo fileInfo = new FileInfo(
        "test.pdf",
        "test",
        "pdf",
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        100
    );
    final ProcessorContext processorContext = new ProcessorContext();
    processorContext.getDocument().setFileInfo(fileInfo);
    assertThat(textPlainProcessor.match(processorContext)).isFalse();
  }
}
