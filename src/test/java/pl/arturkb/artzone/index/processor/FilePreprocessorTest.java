package pl.arturkb.artzone.index.processor;

import static com.google.common.truth.Truth.assertThat;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;
import pl.arturkb.artzone.index.GuiceFactory;

public class FilePreprocessorTest {

  private final GuiceFactory guiceFactory = new GuiceFactory();

  @Test
  public void testReadingFileMetadata() throws Exception {
    final FilePreprocessor filePreprocessor = guiceFactory.create(FilePreprocessor.class);
    final URL resource = getClass().getClassLoader().getResource("FileDtoTestData1.json");
    assertThat(resource).isNotNull();
    final Path path = Paths.get(resource.getPath());
    final ProcessorContext processorContext = new ProcessorContext();
    filePreprocessor.process(processorContext, path);
    assertThat(processorContext.getDocument().getFileInfo()).isNotNull();
    assertThat(processorContext.getDocument().getFileInfo().getSize()).isNotEqualTo(0);
    assertThat(processorContext.getDocument().getFileInfo().getExtension()).isEqualTo("json");
    assertThat(processorContext.getDocument().getFileInfo().getCreationTime()).isNotNull();
    assertThat(processorContext.getDocument().getFileInfo().getLastModifiedTime()).isNotNull();
    assertThat(processorContext.getDocument().getMimeType())
        .isEqualTo(SupportedMimeType.TEXT_PLAIN.getType());
    assertThat((processorContext.getDocument().getMimeSubType()))
        .isEqualTo(SupportedMimeType.TEXT_PLAIN.getSubType());
    assertThat(processorContext.getDocument().getFileInfo().getName())
        .isEqualTo("FileDtoTestData1.json");
    assertThat(processorContext.getDocument().getNumberOfPages()).isEqualTo(0);
    assertThat(processorContext.getDocument().getFileInfo().getPath()).isNotEmpty();
    assertThat(processorContext.getDocument().getTextPlainMetadata()).isNull();
    assertThat(processorContext.getDocument().getDetected()).isTrue();
    assertThat(processorContext.getDocument().getPreprocessed()).isTrue();
    assertThat(processorContext.getDocument().isMultiPage()).isFalse();
  }
}
