package pl.arturkb.artzone.index.command;

import static com.google.common.truth.Truth.assertThat;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import picocli.CommandLine;
import pl.arturkb.artzone.elasticsearch.testing.junit5.RestHighLevelClientFactory;
import pl.arturkb.artzone.elasticsearch.testing.junit5.StandardTransientIndexName;
import pl.arturkb.artzone.elasticsearch.testing.junit5.extensions.ElasticSearchExtensionSupport;
import pl.arturkb.artzone.elasticsearch.testing.junit5.extensions.ElasticSearchResourceExtension;
import pl.arturkb.artzone.index.Configuration;
import pl.arturkb.artzone.index.ExitCode;
import pl.arturkb.artzone.index.GuiceFactory;

@ExtendWith(ElasticSearchExtensionSupport.class)
public class IndexTest {

  private final GuiceFactory guiceFactory = new GuiceFactory();

  private final ElasticSearchResourceExtension elasticSearchResourceExtension;

  IndexTest() throws Exception {
    final Configuration configuration = guiceFactory.create(Configuration.class);
    elasticSearchResourceExtension = new ElasticSearchResourceExtension.Builder()
        .withIndexNameFormat(
            StandardTransientIndexName.make()
                .withFunctionalContent("file")
                .withComponent("ittest")
                .withEnvironment("local")
                .withIndexNameTimeToLiveInSeconds(30)
                .build()
        )
        .withRestHighLevelClient(
            RestHighLevelClientFactory.make()
                .withHostnames(configuration.getElasticHostnames())
                .withPort(configuration.getElasticPort())
                .withUsername(configuration.getElasticUsername())
                .withPassword(configuration.getElasticUsernamePassword())
                .build()
        )
        .build();
  }

  @Test
  public void testHelpWithShortOption() {
    CommandLine commandLine = new CommandLine(Index.class, guiceFactory);
    StringWriter stringWriter = new StringWriter();
    commandLine.setOut(new PrintWriter(stringWriter));
    int exitCode = commandLine.execute("-h");
    assertThat(exitCode).isEqualTo(ExitCode.NORMAL.getValue());
    assertThat(stringWriter.toString()).contains("Usage");
  }

  @Test
  public void testHelpWithLongOption() {
    CommandLine commandLine = new CommandLine(Index.class, guiceFactory);
    StringWriter stringWriter = new StringWriter();
    commandLine.setOut(new PrintWriter(stringWriter));
    int exitCode = commandLine.execute("--help");
    assertThat(exitCode).isEqualTo(ExitCode.NORMAL.getValue());
    assertThat(stringWriter.toString()).contains("Usage");
  }

  @Test
  public void testHappyDay() {
    final URL resource = getClass().getClassLoader().getResource("FileDtoTestData1.json");
    assertThat(resource).isNotNull();
    final String path = resource.getPath().split("FileDtoTestData1.json")[0];
    CommandLine commandLine = new CommandLine(Index.class, new GuiceFactory());
    StringWriter stringWriter = new StringWriter();
    commandLine.setOut(new PrintWriter(stringWriter));
    int exitCode = commandLine
        .execute(
            "-v",
            "-n" + elasticSearchResourceExtension.generateUniqueIndexName(),
            path);
    assertThat(exitCode).isEqualTo(ExitCode.NORMAL.getValue());
  }

  @Test
  public void testIndexNameAlreadyExists() {
    final String indexName = elasticSearchResourceExtension.generateUniqueIndexName();
    final URL resource = getClass().getClassLoader().getResource("FileDtoTestData1.json");
    assertThat(resource).isNotNull();
    final String path = resource.getPath().split("FileDtoTestData1.json")[0];

    CommandLine commandLine1 = new CommandLine(Index.class, new GuiceFactory());
    StringWriter stringWriter1 = new StringWriter();
    commandLine1.setOut(new PrintWriter(stringWriter1));
    int exitCode = commandLine1
        .execute(
            "-v",
            "-n" + indexName,
            path);
    assertThat(exitCode).isEqualTo(ExitCode.NORMAL.getValue());

    CommandLine commandLine2 = new CommandLine(Index.class, new GuiceFactory());
    StringWriter stringWriter2 = new StringWriter();
    commandLine2.setOut(new PrintWriter(stringWriter2));
    exitCode = commandLine2
        .execute(
            "-v",
            "-n" + indexName,
            path);
    assertThat(exitCode).isEqualTo(ExitCode.ELASTIC_INDEX_ALREADY_EXISTS.getValue());
  }
}