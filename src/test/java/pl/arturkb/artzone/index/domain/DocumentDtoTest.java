package pl.arturkb.artzone.index.domain;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import pl.arturkb.artzone.index.DateTimeUtils;
import pl.arturkb.artzone.utils.MappingUtils;

public class DocumentDtoTest {

  private static final SimpleDateFormat FORMATTER = new SimpleDateFormat(DateTimeUtils.PATTERN,
      Locale.ENGLISH);
  private static final String CREATION_TIME = "28-08-2020T10:06:34.000+0000";
  private static final String LAST_MODIFIED_TIME = "28-09-2020T10:06:34.000+0000";

  private static final String JSON_EXPECTED = "{\"path\":\"/home/artur/test.txt\","
      + "\"name\":\"test.txt\","
      + "\"extension\":\"txt\","
      + "\"creationTime\":\"" + CREATION_TIME + "\","
      + "\"lastModifiedTime\":\"" + LAST_MODIFIED_TIME + "\","
      + "\"size\":1000,"
      + "\"pageNumber\":1,"
      + "\"numberOfPages\":10,"
      + "\"content\":\"Just test content\","
      + "\"mimeType\":\"text\","
      + "\"mimeSubType\":\"plain\"}";

  private static DocumentDto DOCUMENT_DTO;

  static {
    try {
      DOCUMENT_DTO = DocumentDto.make()
          .withCreationTime(FORMATTER.parse(CREATION_TIME))
          .withLastModifiedTime(FORMATTER.parse(LAST_MODIFIED_TIME))
          .withExtension("txt")
          .withName("test.txt")
          .withPath("/home/artur/test.txt")
          .withMimeType("text")
          .withMimeSubType("plain")
          .withSize(1000)
          .withNumberOfPages(10)
          .withPageNumber(1)
          .withContent("Just test content")
          .build();
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSerialization() throws JsonProcessingException, JSONException {
    String json = MappingUtils.getJson(DOCUMENT_DTO);
    JSONAssert.assertEquals(JSON_EXPECTED, json, true);
  }

  @Test
  public void testDeserialization() throws JsonProcessingException {
    final DocumentDto documentDto = MappingUtils.getObject(JSON_EXPECTED, DocumentDto.class);
    assertThat(documentDto).isEqualTo(DOCUMENT_DTO);
  }
}
