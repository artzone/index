package pl.arturkb.artzone.index.domain;

import static com.google.common.truth.Truth.assertThat;

import org.junit.jupiter.api.Test;

public class DocumentTest {

  @Test
  public void testGetPage() {
    final Document document = new Document();
    final Page pageUnderTest = new Page(1, "test");
    document.addPage(pageUnderTest);
    assertThat(document.getPage(1)).isEqualTo(pageUnderTest);
  }

  @Test
  public void testGetPageNotFound() {
    final Document document = new Document();
    final Page pageUnderTest = new Page(10, "test");
    document.addPage(pageUnderTest);
    assertThat(document.getPage(1)).isNull();
  }

}
