package pl.arturkb.artzone.index.domain;

import static com.google.common.truth.Truth.assertThat;

import java.time.Instant;
import java.util.Date;
import org.junit.jupiter.api.Test;
import pl.arturkb.artzone.index.processor.SupportedMimeType;

public class DocumentMapperTest {

  private static final String PATH = "/home/artur/test.txt";
  private static final String NAME = "test.txt";
  private static final String EXTENSION = "txt";
  private static final int SIZE = 9000;
  private static final String FIRST_PAGE_CONTENT = "First page content";
  private static final String SECOND_PAGE_CONTENT = "Second page content";

  @Test
  public void testCreateDtoByDocumentAndPageNumber() {
    final DocumentDto firstPage = DocumentMapper.createDto(prepareDocument(), 1);
    assertThat(firstPage.getPath()).isEqualTo(PATH);
    assertThat(firstPage.getName()).isEqualTo(NAME);
    assertThat(firstPage.getExtension()).isEqualTo(EXTENSION);
    assertThat(firstPage.getCreationTime()).isNotNull();
    assertThat(firstPage.getLastModifiedTime()).isNotNull();
    assertThat(firstPage.getSize()).isEqualTo(SIZE);
    assertThat(firstPage.getMimeType()).isEqualTo(SupportedMimeType.TEXT_PLAIN.getType());
    assertThat(firstPage.getMimeSubType()).isEqualTo(SupportedMimeType.TEXT_PLAIN.getSubType());
    assertThat(firstPage.getNumberOfPages()).isEqualTo(2);
    assertThat(firstPage.getPageNumber()).isEqualTo(1);
    assertThat(firstPage.getContent()).isEqualTo(FIRST_PAGE_CONTENT);
  }

  private Document prepareDocument() {
    final FileInfo fileInfo = new FileInfo(
        PATH,
        NAME,
        EXTENSION,
        Date.from(Instant.now()),
        Date.from(Instant.now()),
        SIZE);
    final Document document = new Document();
    document.setFileInfo(fileInfo);
    document.setMimeType(SupportedMimeType.TEXT_PLAIN.getType());
    document.setMimeSubType(SupportedMimeType.TEXT_PLAIN.getSubType());
    document.addPage(new Page(1, FIRST_PAGE_CONTENT));
    document.addPage(new Page(2, SECOND_PAGE_CONTENT));
    return document;
  }

}
