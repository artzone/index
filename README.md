[![Codacy Badge](https://app.codacy.com/project/badge/Grade/4e7c220c1d824970b1d3f4a78f994b9d)](https://www.codacy.com/gl/artzone/index/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=artzone/index&amp;utm_campaign=Badge_Grade)

## Introduction
File tagging and search tool.

## Maven
### Compiling the project
```
./mvnw --settings .m2/settings.xml compile
```

### Building the project
To build the project, you need an ElasticSearch instance running. Simply the integration tests need one instance to run agist to.
 
To start single node ElasticSearch for testing purpose:
1.  Download ElasticSearch version 7.9.2.
2.  Set up environment variables.
```
export ELASTIC_HOSTNAMES="localhost"
export ELASTIC_PORT="9200"
```
3.  Start ElastciSearch with the following command
```  
elasticsearch-7.9.2/bin/elasticsearch -Ediscovery.type=single-node
```
then build the project with
```
./mvnw --settings .m2/settings.xml clean inatll
```
, and finally to build starting script
```
./mvnw --settings .m2/settings.xml package appassembler:assemble
```

### Checking for new plugin updates
```
./mvnw --settings .m2/settings.xml versions:display-plugin-updates
```
### Checking  for updated dependencies in repository
```
./mvnw --settings .m2/settings.xml versions:display-dependency-updates
```
### Checking  for style validation
```
./mvnw  --settings .m2/settings.xml checkstyle:check
```
### Checking  for bugs
```
./mvnw  --settings .m2/settings.xml spotbugs:check
```
  